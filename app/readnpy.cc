#include <libnpy/npy.hpp>
#include <string>
#include <vector>

int main()
{
  const std::string path{"./out.npy"};
  npy::npy_data d = npy::read_npy<double>(path);

  std::vector<double> data = d.data;
  std::vector<unsigned long> shape = d.shape;
  bool fortran_order = d.fortran_order;

  for (auto e : data)
  {
    std::cout << e << std::endl;
  }
}
