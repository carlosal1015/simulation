#include <vector>
#include <iostream>
#include <ranges>

int main()
{
  auto v = std::views::iota(-10, 10);
  auto b = std::vector(v.begin(), v.end());

  for (auto &e : b)
  {
    std::cout << e << std::endl;
  }

  return 0;
}