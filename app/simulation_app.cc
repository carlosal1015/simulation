#include <simulation/simulation.hh>
#include <iostream>
#include <cstdlib>

int main()
{
  int result = simulation::add_one(1);
  std::cout << "1 + 1 = " << result << std::endl;

  return EXIT_SUCCESS;
}