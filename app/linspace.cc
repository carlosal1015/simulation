#include <iostream>
#include <array>
#include <algorithm>
#include <vector>

int main()
{
  float a = 1.2;
  std::vector<double> v(100);
  std::array<double, 100> arr;

  std::generate(v.begin(), v.end(), [n = 0, &a]() mutable
                { return n++ * a; });

  for (auto e : v)
  {
    std::cout << e << std::endl;
  }

  return 0;
}
