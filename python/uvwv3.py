#!/usr/bin/env python


import numpy as np

from mpi4py import MPI

from uvw.parallel import PRectilinearGrid
from uvw import DataArray

comm = MPI.COMM_WORLD
rank = comm.Get_rank()

N = 20

# Domain bounds per rank
bounds = [
    {"x": (-2, 0), "y": (-2, 0)},
    {"x": (-2, 0), "y": (0, 2)},
    {"x": (0, 2), "y": (-2, 2)},
]

# Domain sizes per rank
sizes = [
    {"x": N, "y": N},
    {"x": N, "y": N},
    {"x": N, "y": 2 * N - 1},  # account for overlap
]

# Size offsets per rank
offsets = [
    [0, 0],
    [0, N],
    [N, 0],
]

x = np.linspace(*bounds[rank]["x"], sizes[rank]["x"])
y = np.linspace(*bounds[rank]["y"], sizes[rank]["y"])

xx, yy = np.meshgrid(x, y, indexing="ij", sparse=True)
r = np.sqrt(xx**2 + yy**2)
data = np.exp(-(r**2))

# Indicating rank info with a cell array
proc = np.ones((x.size - 1, y.size - 1)) * rank

with PRectilinearGrid("pgrid.pvtr", (x, y), offsets[rank]) as rect:
    rect.addPointData(DataArray(data, range(2), "gaussian"))
    rect.addCellData(DataArray(proc, range(2), "proc"))
